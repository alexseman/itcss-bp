# ITCSS BP

Boilerplate CSS Framework(ish) package based on Inuit SCSS framework which in turn is based on ITCSS architecture. 

## Installing

You need to have `npm` and `SASS`  installed. Then:

1. Clone the repository:

		git clone https://alexseman@bitbucket.org/alexseman/itcss-bp.git

2. Fetch all the dependencies:

		npm install
 
## Authors

* **Alex Seman** - *Initial work* - [Alex Seman](https://alexseman.com)
